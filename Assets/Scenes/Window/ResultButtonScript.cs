﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class ResultButtonScript : MonoBehaviour {

    [DllImport("__Internal")]
    private static extern void OpenWindow(string str);

    public static int point = 0;

    public void UseAsset()
    {
        //soundManager.Play(SoundManager.SE.CLICK);
        OpenWindow("http://tiatia.bambina.jp/game/NekoAtsuiUnity1Week/asset.html");
    }
    public void Twitter()
    {
        //soundManager.Play(SoundManager.SE.CLICK);
        //Hello();
        //if(point == 0)
        {
            OpenWindow("http://twitter.com/intent/tweet?text=おにぎりを"+ point + "こにぎった http://tiatia.bambina.jp/game/KumaNigiri/");
        }
    }

    public void Ranking()
    {
        naichilab.RankingLoader.Instance.SendScoreAndShowRanking(point);
    }

    public void ResetGame()
    {
        
    }
}
