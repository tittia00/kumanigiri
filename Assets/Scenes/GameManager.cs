﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class GameManager : MonoBehaviour
{

    public SManager sceneManager = null;

    public GameObject[] auras = null;
    public int chargePower = 0;

    public PlayableDirector[] aurasAnime = null;

    public SpriteRenderer background = null;

    public GameObject[] dispCheck = null;

    public void ResetGame()
    {
        for (int i = 0; i < auras.Length; i++)
        {
            auras[i].SetActive(false);
        }
        chargePower = 0;
        auraColor = 1f;
        background.color = new Color(1f, 1f, 1f , 1f);


        auras[0].SetActive(false);
        auras[1].SetActive(false);
        auras[2].SetActive(false);
        auras[3].SetActive(false);
        auras[4].SetActive(false);
        auras[5].SetActive(false);
        auras[6].SetActive(false);
        auras[7].SetActive(false);
        aurasAnime[0].Resume();
        aurasAnime[1].Resume();

        for (int i = 0; i < dispCheck.Length; i++)
        {
            dispCheck[i].SetActive(true);
        }
        sceneManager.ResetGame();
    }

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < auras.Length; i++)
        {
            auras[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public float auraColor = 1f;
    public void addAura(int add)
    {
        chargePower += add;

        if (chargePower >= 255)
        {
            chargePower = 255;
        }

        if (chargePower <= 0)
        {
            auraColor = 0;
        }
        else
        {
            auraColor = chargePower / 255.0f;
        }

        if (auraColor >= 1f)
        {
            auraColor = 1f;
        }

        background.color = new Color(1f, 1.0f - auraColor, 1.0f - auraColor, 1f);

        if (chargePower >= 200)
        {
            auras[0].SetActive(true);
            auras[1].SetActive(true);
            auras[2].SetActive(true);
            auras[3].SetActive(true);
            auras[4].SetActive(true);
            auras[5].SetActive(true);
            auras[6].SetActive(true);
            auras[7].SetActive(true);
            aurasAnime[0].Play();
            aurasAnime[1].Play();

        }
        if (chargePower >= 140)
        {
            auras[0].SetActive(true);
            auras[1].SetActive(true);
            auras[2].SetActive(true);
            auras[3].SetActive(true);
            auras[4].SetActive(true);
            auras[5].SetActive(true);
            auras[6].SetActive(true);
            auras[7].SetActive(true);
            aurasAnime[0].Play();
            aurasAnime[1].Resume();

        }
        else if (chargePower >= 80)
        {
            auras[0].SetActive(true);
            auras[1].SetActive(true);
            auras[2].SetActive(true);
            auras[3].SetActive(true);
            auras[4].SetActive(true);
            auras[5].SetActive(true);
            auras[6].SetActive(false);
            auras[7].SetActive(false);
            aurasAnime[0].Play();
            aurasAnime[1].Resume();

        }
        else if (chargePower >= 50)
        {
            auras[0].SetActive(true);
            auras[1].SetActive(true);
            auras[2].SetActive(true);
            auras[3].SetActive(true);
            auras[4].SetActive(false);
            auras[5].SetActive(false);
            auras[6].SetActive(false);
            auras[7].SetActive(false);
            aurasAnime[0].Play();
            aurasAnime[1].Resume();

        }
        else if (chargePower >= 20)
        {
            auras[0].SetActive(true);
            auras[1].SetActive(true);
            auras[2].SetActive(true);
            auras[3].SetActive(true);
            auras[4].SetActive(false);
            auras[5].SetActive(false);
            auras[6].SetActive(false);
            auras[7].SetActive(false);
            aurasAnime[0].Resume();
            aurasAnime[1].Resume();

        }
        else if (chargePower >= 10)
        {
            auras[0].SetActive(true);
            auras[1].SetActive(true);
            auras[2].SetActive(false);
            auras[3].SetActive(false);
            auras[4].SetActive(false);
            auras[5].SetActive(false);
            auras[6].SetActive(false);
            auras[7].SetActive(false);
        }
        else if (chargePower <= 10)
        {
            auras[0].SetActive(false);
            auras[1].SetActive(false);
            auras[2].SetActive(false);
            auras[3].SetActive(false);
            auras[4].SetActive(false);
            auras[5].SetActive(false);
            auras[6].SetActive(false);
            auras[7].SetActive(false);
        }

    }


    public void Nigiru()
    {
        for (int i=0; i< dispCheck.Length; i++)
        {
            dispCheck[i].SetActive(false);
        }

        sceneManager.SetOnigiriCount((int)(chargePower / 255.0f * 100));

        StartCoroutine("NextScene");
    }

    private IEnumerator NextScene()
    {
        yield return new WaitForSeconds(0.5f);
        sceneManager.NextScene();
        yield return new WaitForSeconds(2.0f);
        sceneManager.NextScene();
        yield return new WaitForSeconds(2.0f);
        sceneManager.NextScene();
    }

}