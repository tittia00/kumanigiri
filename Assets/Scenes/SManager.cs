﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SManager : MonoBehaviour {

    public enum SCENE
    {
        NIGIRI_MAE,
        NIGIRI1,
        NIGIRI2,
        RESULT,
        MAX
    };

    public void ResetGame()
    {
        onigiriCount = 0;
        onigiriCountText.text = onigiriCount + "こ";
        resultItem.SetActive(false);

        ResultButtonScript.point = onigiriCount;
        SetScene(SCENE.NIGIRI_MAE);
    }

    public GameObject onigiri = null;
    public int onigiriCount = 1;
    public GameObject[] onigiris;


    public GameObject resultItem = null;
    public Text onigiriCountText;

    public void SetOnigiriCount(int count)
    {
        //sceneItems
        onigiriCount = count;
        if(onigiriCount <= 0)
        {
            onigiriCount = 0;
        }
    }

    public GameObject[] sceneItems = null;
    SCENE current;

	// Use this for initialization
	void Start () {
        ResetGame();
    }

    public void NextScene()
    {
        current++;
        if (current >= SCENE.MAX)
        {
            current = SCENE.RESULT;
            return;
        }
        SetScene(current);

        if (current >= SCENE.RESULT)
        {
            
            // おにぎりの設定
            for (int i=0; i < onigiriCount; i++)
            {
                onigiris[i].SetActive(true);
            }

            resultItem.SetActive(true);
            onigiriCountText.text = onigiriCount + "こ";
            ResultButtonScript.point = onigiriCount;
        }
    }

    public void SetScene(SCENE sceneIndex)
    {
        current = sceneIndex;
        for (int i = 0; i < sceneItems.Length; i++)
        {
            sceneItems[i].SetActive(false);
        }
        sceneItems[(int)current].SetActive(true);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
